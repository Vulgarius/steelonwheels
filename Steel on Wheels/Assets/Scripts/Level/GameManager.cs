﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UI;

namespace Level
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager instance; 
        private GameObject[] _cars;
        private InGame _ig;
        
        List<RankingSystem> sortArray = new List<RankingSystem>();

        public int pass; // quando o carro chega ao ultimo checkPoint
        public bool _enterArea;
        public float _timeToEnterArea = 10f;
        public bool finish;

        public string firstPlace, secondPlace, thirdPlace, fourthPlace;
        private GameObject[] _gameObjects;

        private void Awake()
        {
            instance = this;
            _cars = GameObject.FindGameObjectsWithTag("Vehicle");
            _ig = FindObjectOfType<InGame>();
        }

        void Start()
        {
           
            for (int i = 0; i < _cars.Length; i++)
            {
                sortArray.Add(_cars[i].GetComponent<RankingSystem>()); // assigna cada caso de modo a poder depois verificar quem esta em primeiro
              
            }
        }

       
        void Update()
        {
            CalculateRank();
        }
        
        

        private void CalculateRank() //calculo da posicao de cada carro consoante o valor da variavel counter
        {
            sortArray = sortArray.OrderBy(x => x.counter).ToList();   // OrderBy vai colocar cada jogar em ordem crescente

           
            switch (sortArray.Count)
            {
                case 4: 
                    sortArray[0].rank = 4;
                    sortArray[1].rank = 3;
                    sortArray[2].rank = 2;
                    sortArray[3].rank = 1;

                    _ig.a = sortArray[3].name;
                    _ig.b = sortArray[2].name;
                    _ig.c = sortArray[1].name;
                    _ig.d = sortArray[0].name;
                    break;
                
                case 3:
                    
                    sortArray[0].rank = 3;
                    sortArray[1].rank = 2;
                    sortArray[2].rank = 1;
                    
                    _ig.a = sortArray[2].name;
                    _ig.b = sortArray[1].name;
                    _ig.c = sortArray[0].name;
                    break;
                
                case 2:
                    sortArray[0].rank = 2;
                    sortArray[1].rank = 1;
                    
                    _ig.a = sortArray[1].name;
                    _ig.b = sortArray[0].name;
                    break;
                
                case 1:
                    sortArray[0].rank = 1;
                    
                    _ig.a = sortArray[0].name;
                    
                    if (firstPlace == "")
                    {
                        firstPlace = sortArray[0].name;
                        
                    }

                    break;
                    
            }
          
            
                if (pass >= (float) _cars.Length / 2) // se dois jogadores chegarem a area e o tempo
                {
                    pass = 0;                        // apenas chama uma vez
                    sortArray = sortArray.OrderBy(t => t.counter).ToList();

                    foreach (RankingSystem rs in sortArray)
                    {
                        if (rs.rank == sortArray.Count) // verifica posicoes
                        {
                            if (rs.gameObject.name == "Vehicle") //se o jogador morrer
                            {
                                //GameOverScence
                            }
                            if (fourthPlace == "")
                            {
                                fourthPlace = rs.gameObject.name;
                            }
                            if (thirdPlace == "")
                            {
                                thirdPlace = rs.gameObject.name;
                            }
                            else if(secondPlace == "")
                            {
                                secondPlace = rs.gameObject.name;
                            }
                            rs.gameObject.SetActive(false); // remove ultimo do jogo
                        }
                    }

                   
                    sortArray.Clear();

                    for (int i = 0; i < _cars.Length; i++)
                    {
                        sortArray.Add(_cars[i].GetComponent<RankingSystem>());
                    }
                    
                    
                    
                    // se detetou o ultimo checkPoint 
                    CurrentCheckPoint cp = new CurrentCheckPoint();
                    
                    if (cp.currentCheckPNumber == 15)
                    {
                        _enterArea = true; // possivel entrar na area
                        _timeToEnterArea--;  // tempo diminuiu
                    }

                    if (_timeToEnterArea <= 0)
                    {
                        _enterArea = false;   //fecha as portas
                        // destoi carros que nao entraram
                      
                    }
                }

                if (_cars.Length < 2) // gameOverScene 
                {
                    finish = true; 
                    //chama a scene de gameOver
                }
        }
    }
}



