﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

public class SceneBehaviour : MonoBehaviour
{
    // Start is called before the first frame update

    public int SceneToLoad;
    void Start()
    {
        SceneManager.LoadSceneAsync(SceneToLoad, LoadSceneMode.Additive);
    }
}
