﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node_Running : AI_Node
{
    public Node_Running(int agression, int friendliness) :
        base(agression, friendliness)
    {

    }

    public override void CarRunning()
    {
        for (int i = 0; i < vehicle_eyes.Length; i++)
        {
            if ((Physics.Raycast(vehicle_script.gameObject.transform.position, transform.TransformDirection(((i - 8) * 4.5f), 0, 1), out vehicle_eyes[i], 30))
                 && (vehicle_eyes[i].collider.gameObject.tag == "Vehicle"))
            {
                Debug.DrawRay(vehicle_script.gameObject.transform.position, transform.TransformDirection(((i - 8) * 4.5f), 0, 1) * vehicle_eyes[i].distance, Color.yellow);
                /*
                l = limite
                x = variável
                Função que determina se número gerado está dentro de um certo alcance => ((l - 2x) / 2l)
                */
                if (System.Math.Floor((double)System.Math.Abs((2 - (2 * (aggression * rng.Next(0, 2)))) / 4)) == 1) //Baseado na agressão, aleatoriamente causa o oponenete a trocar de comportamento para um de ataque
                {
                    active_node = 2;
                    break;
                }
            }
            else
            {
                Debug.DrawRay(vehicle_script.gameObject.transform.position, transform.TransformDirection(((i - 8) * 4.5f), 0, 1) * vehicle_eyes[i].distance, Color.white);
            }
        }
    }
}
