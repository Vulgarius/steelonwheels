﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AI_Node : MonoBehaviour
{
    [Range(0, 3)] public int active_node; //Nodo atualmente ativado
    protected RaycastHit[] vehicle_eyes = new RaycastHit[8]; //O jeito que o veículo vê em volta de si
    public Vehicle vehicle_script; //Script que cuida do veículo
    protected System.Random rng = new System.Random();

    [Header("Behaviour Parameters")]
    [Range(1, 10)] public int aggression; //Agressividade do carro inimigo
    [Range(1, 10)] public int friendliness; //Companherismo do carro inimigo

    public AI_Node(int agression, int friendliness)
    {
        this.aggression = agression;
        this.friendliness = friendliness;
    }

    public virtual void CarRunning() //Se o carro está apenas correndo
    {

    }

    public virtual void CarFighting() //Se o carro está apenas lutando
    {

    }

    public virtual void CarMixed() //Se o carro está correndo e lutando
    {

    }

    public virtual void CarHelping() //Se o carro está ajudando
    {

    }
}
