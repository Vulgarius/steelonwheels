﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunScript : MonoBehaviour
{
    [Header("Components")]
    [SerializeField]
    GameObject bulletPrefab; // tipo de prefab que vamos instanciar
    [Range (0, 1000)] public int ammo; //Quantidade de munição

    [SerializeField]
    Transform bulletSpawnPoint; // aceder onde a bala vai ser instanciada

    [Header("Detection")]
    private RaycastHit[] aim = new RaycastHit[5]; //Raycast responsável por garantir que haja algo na frente do jogador

    [Header("Controls")]
    public KeyCode keyFire; //Botão usado para atirar

    [Header("Test")]
    [Range (0, 3)] public int gun_type; //Que tipo de arma está sendo utilizada
    public Bullet bullet_script; //Referencia o script da bala


    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        switch (gun_type)
        {
            case 0 : //Se o botão de tiro puder ser segurado
                if (Input.GetKey(keyFire))
                {
                    FireControl();
                }
                break;
            case 1: //Se só atira quando o botão é pressionado
                if (Input.GetKeyDown(keyFire)) 
                {
                    FireControl();
                }
                break;
            case 2 : //Se o botão de tiro ser segurado permite carregar o tiro
                if (Input.GetKeyUp(keyFire))
                {
                    FireControl();
                }
                if (Input.GetKey(keyFire))
                {

                }
                break;
            case 3 : //Se a arma mira em um oponente
                if (Input.GetKeyDown(keyFire))
                {
                    FireControl();
                }
                break;
        }
    }

    void FixedUpdate()
    {
        if (gun_type == 3) //Se a arma é teleguiada
        {
            for (int i = 0; i < aim.Length; i++)
            {
                if ((Physics.Raycast(bulletSpawnPoint.transform.position, transform.TransformDirection(((i - 2) * 0.2f), 0, 1), out aim[i], 20)) 
                    && (aim[i].collider.gameObject.tag == "Vehicle")) //Envia cinco raios, testando se atingem algo com a tag "Vehicle"
                {
                    Debug.DrawRay(transform.position, transform.TransformDirection(((i - 2) * 0.2f), 0, 1) * aim[i].distance, Color.yellow);
                    bullet_script.Aiming(aim[i].transform); //Prende a mira ao alvo
                }
                else
                {
                    Debug.DrawRay(transform.position, transform.TransformDirection(((i - 2) * 0.2f), 0, 1) * aim[i].distance, Color.white);
                    bullet_script.Aiming(null); //Desativa a mira
                }
            }
        }
    }

    void FireControl()
    {
        if((bulletPrefab) && (bulletSpawnPoint) && (ammo > 0)) // se a bala estiver disponivel e ainda houver munição
        {
            // instancio a bala , a posicao de instanciacao colocada na pistola com rotacao horiginal * a rotacao do instanciador
            Instantiate(bulletPrefab, bulletSpawnPoint.position, bulletSpawnPoint.rotation * bulletPrefab.transform.rotation);
            ammo--; //Diminui a quantidade de munição
        }
        
    }
}
